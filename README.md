# WP Strike for Gaza

A WordPress plugin that redirects to a page that shows a poster in support of Gaza instead of the usual web contents until a certain date / time.

Built for <a href="https://bit.ly/strajk-za-gazu">bit.ly/strajk-za-gazu</a>.

## Instructions

Download repository as zip and install to WordPress. 

Avaliable settings: 

- pick a time to stop redirecting 
- choose an image (default image included)
- choose to redirect just the homepage or all pages 

## Licenses
Code is GPLv2.

Image in images directory is property of the <a href="https://bit.ly/strajk-za-gazu">bit.ly/strajk-za-gazu</a> initiative.
