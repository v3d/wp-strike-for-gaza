<?php
/*
Plugin Name: WP Strike for Gaza
Description: Redirects to a page that shows support for Gaza until a certain date / time.
Version: 1.0
Author: v3d
*/

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

// Add settings menu
add_action('admin_menu', 'sfg_add_admin_menu');
add_action('admin_init', 'sfg_settings_init');
add_action('admin_enqueue_scripts', 'sfg_enqueue_media_uploader');

function sfg_add_admin_menu() {
    add_options_page('Strike for Gaza', 'Strike for Gaza', 'manage_options', 'homepage_redirect', 'sfg_options_page');
}

function sfg_settings_init() {
    register_setting('sfg_settings_group', 'sfg_settings', 'sfg_settings_validate');

    add_settings_section(
        'sfg_settings_section',
        __('Strike for Gaza Settings', 'sfg'),
        'sfg_settings_section_callback',
        'sfg_settings_group'
    );

    add_settings_field(
        'sfg_datetime',
        __('Strike for Gaza End Date and Time', 'sfg'),
        'sfg_datetime_render',
        'sfg_settings_group',
        'sfg_settings_section'
    );

    add_settings_field(
        'sfg_image',
        __('Redirect Image', 'sfg'),
        'sfg_image_render',
        'sfg_settings_group',
        'sfg_settings_section'
    ); 
    
    add_settings_field(
        'sfg_redirect_all',
        __('Redirect All Pages', 'sfg'),
        'sfg_redirect_all_render',
        'sfg_settings_group',
        'sfg_settings_section'
    );

    add_settings_field(
        'sfg_info',
        __('For more images visit:<br><a href="https://bit.ly/strajk-za-gazu-vizuali">bit.ly/strajk-za-gazu-vizuali</a>', 'sfg'),
        'sfg_info_render',
        'sfg_settings_group',
        'sfg_settings_section'
    );
}

function sfg_datetime_render() {
    $options = get_option('sfg_settings');
    $server_current_datetime = current_time('Y-m-d\TH:i'); // Get the server's current date and time in 'datetime-local' format
    ?>
    <input type='datetime-local' name='sfg_settings[sfg_datetime]' value='<?php echo isset($options['sfg_datetime']) ? esc_attr($options['sfg_datetime']) : ''; ?>'>
    <div id='current_datetime' style='margin-top:10px;'>Current Server Date and Time: <?php echo esc_html(date("Y-m-d H:i:s", strtotime(current_time('mysql')))); ?></div>
    <?php
}

function sfg_image_render() {
    $options = get_option('sfg_settings');
    $image = isset($options['sfg_image']) ? esc_url($options['sfg_image']) : plugin_dir_url(__FILE__) . 'images/PLAKAT_UGASENO_JPEG.jpg';
    ?>
    <input type='text' id='sfg_image' name='sfg_settings[sfg_image]' value='<?php echo $image; ?>' class='regular-text'>
    <input type='button' class='button-primary' value='Select Image' id='sfg_image_button' />
    <div id='sfg_image_preview' style='margin-top:10px;'>
        <img src='<?php echo $image; ?>' style='max-width:300px;'>
    </div>
    <script>
        jQuery(document).ready(function($){
            var mediaUploader;
            $('#sfg_image_button').click(function(e) {
                e.preventDefault();
                if (mediaUploader) {
                    mediaUploader.open();
                    return;
                }
                mediaUploader = wp.media.frames.file_frame = wp.media({
                    title: 'Choose Image',
                    button: {
                        text: 'Choose Image'
                    }, multiple: false });
                mediaUploader.on('select', function() {
                    var attachment = mediaUploader.state().get('selection').first().toJSON();
                    $('#sfg_image').val(attachment.url);
                    $('#sfg_image_preview').html('<img src="'+attachment.url+'" style="max-width:300px;">');
                });
                mediaUploader.open();
            });
        });
    </script>
    <?php
}

function sfg_redirect_all_render() {
    $options = get_option('sfg_settings');
    $checked = isset($options['sfg_redirect_all']) ? checked(1, $options['sfg_redirect_all'], false) : '';
    ?>
    <input type='checkbox' id='sfg_redirect_all' name='sfg_settings[sfg_redirect_all]' value='1' <?php echo $checked; ?>>
    <?php
}

function sfg_info_render() {
    // This function can be empty as the text is already added in the add_settings_field function.
}

function sfg_settings_section_callback() {
    echo __('Set the date and image for the strike for Gaza. <br><br> More info @ <a href="https://bit.ly/strajk-za-gazu">bit.ly/strajk-za-gazu</a>', 'sfg');
}

function sfg_options_page() {
    if (!current_user_can('manage_options')) {
        return;
    }
    ?>
    <form action='options.php' method='post'>
        <h2>Strike for Gaza</h2>
        <?php
        settings_fields('sfg_settings_group');
        do_settings_sections('sfg_settings_group');
        submit_button();
        ?>
    </form>
    <?php
}

function sfg_enqueue_media_uploader() {
    wp_enqueue_media();
}

// Redirect function
add_action('template_redirect', 'sfg_redirect_homepage');

function sfg_redirect_homepage() {
    if (!is_admin()) {
        $options = get_option('sfg_settings');
        $end_datetime = isset($options['sfg_datetime']) ? strtotime($options['sfg_datetime']) : 0;
        $current_datetime = strtotime(current_time('mysql')); // Get the current server time

        $image_url = isset($options['sfg_image']) ? esc_url($options['sfg_image']) : plugin_dir_url(__FILE__) . 'images/PLAKAT_UGASENO_JPEG.jpg';
        $title = get_bloginfo('name'); // Use the site's title

        // Debug output to ensure correct time comparison
        echo "<!-- Current Server Time: " . esc_html(date('Y-m-d H:i:s', $current_datetime)) . " -->";
        echo "<!-- Redirect End Time: " . esc_html(date('Y-m-d H:i:s', $end_datetime)) . " -->";

        // Check if redirect all pages option is enabled
        $redirect_all = isset($options['sfg_redirect_all']) && $options['sfg_redirect_all'] == 1;

        if ($current_datetime < $end_datetime) {
            if ($redirect_all || is_front_page()) {
                include(plugin_dir_path(__FILE__) . 'redirect-page.php');
                exit;
            }
        }
    }
}
?>
