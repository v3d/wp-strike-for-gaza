<!DOCTYPE html>
<html>
<head>
    <title><?php echo esc_html($title); ?></title>
    <style>
        body, html {
            height: 100%;
            margin: 0;
            display: flex;
            justify-content: center;
            align-items: center;
        }
        .bg {
            background-color: black;
            background-image: url('<?php echo esc_url($image_url); ?>');
            height: 100vh;
            width: 100vw;
            background-position: center;
            background-repeat: no-repeat;
            background-size: contain;
        }
    </style>
</head>
<body>
    <div class="bg"></div>
</body>
</html>
